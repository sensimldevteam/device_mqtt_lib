/*==========================================================
*
*-  Copyright Notice  -------------------------------------
*
*    Licensed Materials - Property of QuickLogic Corp.
*    Copyright (C) 2019 QuickLogic Corporation
*    All rights reserved
*    Use, duplication, or disclosure restricted
*
*    File   : MinorTopicDispatch_System.c
*    Purpose: System minor topic dispatch
*
*=========================================================*/
#include <stdio.h>
#include <string.h>
#include "Mqttsn_globals.h"
#include "Mqttsn_MessageHandler.h"
#include "Mqttsn_Topics.h"
#include "MinorTopicDispatch_Storage.h"
#include "MajorTopicDispatch.h"
#include "MinorTopicDispatch_System.h"
#include "MinorTopicDispatch_Collect.h"

#include "MinorTopicDispatch_Live.h"

#define TOPIC_COMPDATE_LENGTH 20
#define TOPIC_VERSION_LENGTH 20
#define TOPIC_UUIDS_LENGTH 32

/* dispatch entry */

void do_all_stop(Mqttsn_IOMsgData_t *pIoMsgData)
{
    //We have disconnected from the network, stop everything
    Live_AllStop(pIoMsgData);

#if SENSIML_INTERFACE_ENABLE_RECOGNITION
    Recognition_AllStop(pIoMsgData);
#endif //#if SENSIML_INTERFACE_ENABLE_RECOGNITION

#if SENSIML_CAPTURE_CAN_COLLECT_FLASH_SD

    Collect_AllStop(pIoMsgData);
#endif //SENSIML_CAPTURE_CAN_COLLECT_FLASH_SD

#if SENSIML_SYSTEM_SUPPORTS_FILE_TRANSFER
    Storage_AllStop();
#endif //SENSIML_SYSTEM_SUPPORTS_FILE_TRANSFER
}

void do_uuids_req(Mqttsn_IOMsgData_t *pIoMsgData)
{
    //No error to send back

    //reply with a Publish payload
    int result = 0;
    Mqttsn_MsgData_t *pOutMsgData = &pIoMsgData->outgoingResponse;

    result = Mqttsn_AllocPublishPayload(MQTTSN_BUFFER_SMALL, &pOutMsgData->pMsgPayldBuf,
                                        &pOutMsgData->allocLen);

    if (result)
    {
        pOutMsgData->topicId = TOPIC_SYS_DEVICE_UUIDS_RSP;
        pOutMsgData->payldLen = TOPIC_UUIDS_LENGTH;

        memcpy((void *)pOutMsgData->pMsgPayldBuf, DeviceClassUUID, UUID_TOTAL_BYTES);

        //DUNIQUE_UUID left at 0s for now
    }
    else
    {
        printf("Error: do_uuids_req - buffer not allocated");
    }
}

/* handle get sw version command */
void do_get_version(Mqttsn_IOMsgData_t *pIoMsgData)
{
    //No error to send back

    //reply with a Publish payload
    int result = 0;
    Mqttsn_MsgData_t *pOutMsgData = &pIoMsgData->outgoingResponse;

    result = Mqttsn_AllocPublishPayload(MQTTSN_BUFFER_SMALL, &pOutMsgData->pMsgPayldBuf,
                                        &pOutMsgData->allocLen);

    if (result)
    {
        uint8_t *pBuf = pOutMsgData->pMsgPayldBuf;

        pOutMsgData->topicId = TOPIC_SYS_VERSION_RSP;
        pOutMsgData->payldLen = strlen(SOFTWARE_VERSION_STR);

        mqtt_reply_string(pBuf, SOFTWARE_VERSION_STR, NULL);
    }
    else
    {
        printf("Error: do_get_version - buffer not allocated");
    }
}

/* handle COMPILER date/time stamp command */
void do_get_compdatetime(Mqttsn_IOMsgData_t *pIoMsgData)
{
    //No error to send back

    //reply with a Publish payload
    int result = 0;
    Mqttsn_MsgData_t *pOutMsgData = &pIoMsgData->outgoingResponse;

    result = Mqttsn_AllocPublishPayload(MQTTSN_BUFFER_SMALL, &pOutMsgData->pMsgPayldBuf,
                                        &pOutMsgData->allocLen);

    if (result)
    {
        uint8_t *pPayld = pOutMsgData->pMsgPayldBuf;
        uint32_t buffLen = 0;

        pOutMsgData->topicId = TOPIC_SYS_COMPDATETIME_RSP;

        strcpy((char *)&pPayld[buffLen], __DATE__);
        buffLen += strlen(__DATE__);

        pPayld[buffLen++] = ' ';

        strcpy((char *)&pPayld[buffLen], __TIME__);
        buffLen += strlen(__TIME__);

        pOutMsgData->payldLen = buffLen;
    }
    else
    {
        printf("Error: do_get_version - buffer not allocated");
    }
}

void do_set_unixtime(Mqttsn_IOMsgData_t *pIoMsgData)
{
    uint32_t unixTime = Mqttsn_BuffRead_u32(&pIoMsgData->incomingMsg.pMsgPayldBuf);
    printf("unix time: %ld\r\n", unixTime);
    system_user_set_time(unixTime);
}

void do_get_status(Mqttsn_IOMsgData_t *pIoMsgData)
{
    //No error to send back

    //reply with a Publish payload
    int result = 0;
    Mqttsn_MsgData_t *pOutMsgData = &pIoMsgData->outgoingResponse;

    result = Mqttsn_AllocPublishPayload(MQTTSN_BUFFER_SMALL, &pOutMsgData->pMsgPayldBuf,
                                        &pOutMsgData->allocLen);

    if (result)
    {
        uint8_t *pPayld = pOutMsgData->pMsgPayldBuf;
        pOutMsgData->payldLen = 0;

        pOutMsgData->topicId = TOPIC_SYS_STATUS_RSP;

        pOutMsgData->payldLen += Mqttsn_BuffWr_u32(&pPayld, mqttsn_globals.cur_status.bytes_saved);
        pOutMsgData->payldLen += Mqttsn_BuffWr_u32(&pPayld, mqttsn_globals.cur_status.bit_flags);
        pOutMsgData->payldLen += Mqttsn_BuffWr_u16(&pPayld, mqttsn_globals.cur_status.rx_count);
        pOutMsgData->payldLen += Mqttsn_BuffWr_u16(&pPayld, mqttsn_globals.cur_status.tx_count);
        pOutMsgData->payldLen += Mqttsn_BuffWr_u16(&pPayld, mqttsn_globals.cur_status.live_oe_count);
        pOutMsgData->payldLen += Mqttsn_BuffWr_u16(&pPayld, mqttsn_globals.cur_status.collect_oe_count);
        pOutMsgData->payldLen += Mqttsn_BuffWr_u8(&pPayld, mqttsn_globals.cur_status.sticky_error_code);
        pOutMsgData->payldLen += Mqttsn_BuffWr_u8(&pPayld, mqttsn_globals.cur_status.error_count);
    }
}

void do_clr_status(Mqttsn_IOMsgData_t *pIoMsgData)
{
    memset(&(mqttsn_globals.cur_status), 0, sizeof(mqttsn_globals.cur_status));
}

/* sensor commands we support - in a table */
static struct system_cmd_dispatch_entry const sys_cmd_table[] = {
    {.value = TOPIC_SYS_ALL_STOP, do_all_stop, .expPayldLength = 0, .isVariableLen = 0},
    {.value = TOPIC_SYS_DEVICE_UUIDS_REQ, do_uuids_req, .expPayldLength = 0, .isVariableLen = 0},
    {.value = TOPIC_SYS_VERSION_REQ, do_get_version, .expPayldLength = 0, .isVariableLen = 0},
    {.value = TOPIC_SYS_COMPDATETIME_REQ, do_get_compdatetime, .expPayldLength = 0, .isVariableLen = 0},
    {.value = TOPIC_SYS_UNIXTIME_SET, do_set_unixtime, .expPayldLength = 4, .isVariableLen = 0},
    {.value = TOPIC_SYS_STATUS_REQ, do_get_status, .expPayldLength = 0, .isVariableLen = 0},
    {.value = TOPIC_SYS_STATUS_CLR, do_clr_status, .expPayldLength = 0, .isVariableLen = 0},
    /* terminate */
    {.value = -1, .handler = NULL}};

//  Dispatch the minor topic
void run_system_cmd(Mqttsn_IOMsgData_t *pIoMsgData)
{
    Mqttsn_MsgData_t *pInMsg = &pIoMsgData->incomingMsg;

    const struct system_cmd_dispatch_entry *pSCDE;

    //if its a PUBACK, we have no use for it here just return
    if (pInMsg->msgType == MQTTSN_PUBACK)
        return;

    for (pSCDE = sys_cmd_table; pSCDE->handler != NULL; pSCDE++)
    {
        if (pSCDE->value == pInMsg->topicId)
        {
            uint32_t payldErr = CheckPayloadValidity(&pIoMsgData->incomingMsg,
                                                     pSCDE->expPayldLength,
                                                     pSCDE->isVariableLen);
            if (!payldErr)
            {
                (*(pSCDE->handler))(pIoMsgData);
                return;
            }
            else
            {
                //there is a payload error, send an error message back
                PopulateRejectMsg(pIoMsgData, SYS_ERR_EINVAL);
            }
        }
    }

    /* unknown */
    set_sys_error(SYS_ERR_ENOTSUP, pInMsg->topicId);
}
