
/*==========================================================
*
*-  Copyright Notice  -------------------------------------
*
*    Licensed Materials - Property of QuickLogic Corp.
*    Copyright (C) 2019 QuickLogic Corporation
*    All rights reserved
*    Use, duplication, or disclosure restricted
*
*    File   : MajorTopicDispatch.c
*    Purpose: APIs to dispatch major topics
*
*=========================================================*/
#include <string.h>
#include <stdio.h>
#include "stdarg.h"
#include "Mqttsn_globals.h"
#include "Mqttsn_MessageHandler.h"
#include "Mqttsn_Topics.h"
#include "MajorTopicDispatch.h"
#include "MinorTopicDispatch_System.h"
#include "MinorTopicDispatch_Collect.h"
#include "MinorTopicDispatch_Storage.h"

struct mqttsn_globals mqttsn_globals;

/******************* Command Table *****************************
 * Containts the commands/hjandler functions for major topics
 * supported by the system
 * Should not be modified!
 *
 ***************************************************************/
static SensiMLMajorTpcDisptach_t MajorCmdsTbl[] = {
    {.value = TOPIC_MAJOR_SYSTEM, .handler = run_system_cmd},
    {.value = TOPIC_MAJOR_LIVE, .handler = run_livestream_cmd},
    {.value = TOPIC_MAJOR_RESULT, .handler = run_livestream_cmd},
#if defined(SENSIML_INTERFACE_ENABLE_RECOGNITION) && SENSIML_INTERFACE_ENABLE_RECOGNITION
    {.value = TOPIC_MAJOR_RECOG, .handler = run_recognition_cmd},
#endif //#if SENSIML_INTERFACE_ENABLE_RECOGNITION
#if defined(SENSIML_SYSTEM_SUPPORTS_FILE_TRANSFER) && SENSIML_SYSTEM_SUPPORTS_FILE_TRANSFER
    {.value = TOPIC_MAJOR_STORAGE, .handler = run_storage_cmd},
#endif //SENSIML_SYSTEM_SUPPORTS_FILE_TRANSFER
#if defined(SENSIML_INTERFACE_ENABLE_COLLECTION) && SENSIML_INTERFACE_ENABLE_COLLECTION
    {.value = TOPIC_MAJOR_COLLECT, .handler = run_collect_cmd},
    {.value = TOPIC_MAJOR_SENSOR, .handler = run_sensor_cmd},
#endif //SENSIML_INTERFACE_ENABLE_COLLECTION

    {.value = 255, .handler = NULL}};

/* send this packet to the BLE */
void my_ble_send(int cmd, int len, const void *data)
{
    uint8_t databuf[20];
    if (len > 20)
    {
        len = 20;
    }
    databuf[0] = cmd;
    memcpy((void *)(&databuf[1]), (void *)(data), len);
    //SendToBLE(SEND_BLE_IMMEDIATE, len + 1, databuf);
}

/* reply to host as a series of strings */
void mqtt_reply_string(uint8_t *pBuf, const char *cp, ...)
{
    va_list ap;

    char *d;
    int n;
    int c;

    va_start(ap, cp);
    n = 0;
    d = (char *)pBuf;

    while ((n < sizeof(mqttsn_globals.u_rsp.misc.u.cbuf)) && (cp != NULL))
    {
        c = *cp++;
        if (c == 0)
        {
            c = ' ';
            cp = va_arg(ap, const char *);
        }
        *d++ = c;
        n++;
    }
}

void PopulateRejectMsg(Mqttsn_IOMsgData_t *pIoMsgData, sys_error_code_t ftErr)
{
    int result = 0;
    Mqttsn_MsgData_t *pOutMsgData = &pIoMsgData->outgoingResponse;
    pOutMsgData->payldLen = 0;

    // get the ack and change return a REJECT
    pIoMsgData->incomingMsg.retCode = MQTTSN_NOT_SUPPORTED;

    //PUBLISH an error
    pOutMsgData->topicId = TOPIC_SYS_ERROR;
    result = Mqttsn_AllocPublishPayload(MQTTSN_BUFFER_SMALL, &pOutMsgData->pMsgPayldBuf,
                                        &pOutMsgData->allocLen);

    if (result)
    {
        uint8_t *pPayld = pOutMsgData->pMsgPayldBuf;
        Mqttsn_TopicInfo_t *pTopic = Mqttsn_GetTopicInfo(pIoMsgData->incomingMsg.topicId);

        pOutMsgData->payldLen += Mqttsn_BuffWr_u16(&pPayld, pIoMsgData->incomingMsg.pktId);
        pOutMsgData->payldLen += Mqttsn_BuffWr_u16(&pPayld, pTopic->assgndID.data.id);
        pOutMsgData->payldLen += Mqttsn_BuffWr_u16(&pPayld, pIoMsgData->incomingMsg.topicId);
        pOutMsgData->payldLen += Mqttsn_BuffWr_u8(&pPayld, ftErr);
        pOutMsgData->payldLen += Mqttsn_BuffWr_u32(&pPayld, 0);
    }

    //Also set the system error
    set_sys_error(ftErr, 0);
}

/* an error has occured .. update error bits */
void set_sys_error(sys_error_code_t errcode, uint32_t more_info)
{
    (void)(more_info);
    if (0 == mqttsn_globals.cur_status.sticky_error_code)
    {
        mqttsn_globals.cur_status.sticky_error_code = errcode;
    }
    mqttsn_globals.cur_status.bit_flags |= ANY_ERROR;
    mqttsn_globals.cur_status.error_count++;
}

/* start sending data - track overrun if needed */
int send_data_start(void)
{
    if (mqttsn_globals.data_busy)
    {
        mqttsn_globals.cur_status.bit_flags |= LIVESTREAM_OVERRUN;
        mqttsn_globals.cur_status.live_oe_count += 1;
        return -1;
    }

    mqttsn_globals.data_busy = 1;
    memset((void *)(&mqttsn_globals.u_data), 0, sizeof(mqttsn_globals.u_data));

    return 0;
}

void send_data_end(void)
{
    mqttsn_globals.data_busy = 0;
}

uint32_t CheckPayloadValidity(Mqttsn_MsgData_t *pInputMsg, uint32_t expPayldLen, uint8_t isVariableLen)
{
    uint32_t result = 0;

    if (isVariableLen)
    {
        if (expPayldLen > pInputMsg->payldLen)
        {
            //send an error publish for unexpected payload length
            //FUTURE:Send a reject publish back
            result = 1;
        }
    }
    else
    {
        if (expPayldLen != pInputMsg->payldLen)
        {
            //send an error publish for unexpected payload length
            //FUTURE:Send a reject publish back
            result = 1;
        }
    }

    return result;
}

void MajorTopicDispatch(Mqttsn_IOMsgData_t *pIoMsgData)
{
    const SensiMLMajorTpcDisptach_t *pMjCmdDisp;
    Mqttsn_MsgData_t *pInMsgData = &pIoMsgData->incomingMsg;

    mqttsn_globals.cur_status.rx_count += 1;

    pMjCmdDisp = &MajorCmdsTbl[0];

    while (pMjCmdDisp->handler != NULL)
    {
        if (pMjCmdDisp->value != GET_TOPIC_MAJOR(pInMsgData->topicId))
        {
            pMjCmdDisp++;
            continue;
        }
        else
        {
            break;
        }
    }

    if (pMjCmdDisp->handler)
    {
        (*(pMjCmdDisp->handler))(pIoMsgData);
    }
    else
    {
        set_sys_error(SYS_ERR_ENOTSUP, mqttsn_globals.cmd_from_host.cmd);
    }
}
