/*==========================================================
*
*-  Copyright Notice  -------------------------------------
*
*    Licensed Materials - Property of QuickLogic Corp.
*    Copyright (C) 2019 QuickLogic Corporation
*    All rights reserved
*    Use, duplication, or disclosure restricted
*
*    File   : MinorTopicDispatch_Sensor.c
*    Purpose: Sensor minor topic dispatch
*
*=========================================================*/
#include <stdio.h>
#include <string.h>
#include "stdarg.h"

#include "Mqttsn_Topics.h"
#include "MajorTopicDispatch.h"
#include "MinorTopicDispatch_Sensor.h"
#include "MinorTopicDispatch_System.h"
#include "SensorTopicUser.h"

/*
 * This file is the "dispatch" for all SENSOR commands.
 * The MAJOR item SENSOR has already be dispatched.
 * Here, we are handling the MINOR sensor commands.
 */

struct sensor_config_msg recent_cfg_msg;
struct sensor_config_msg sensor_configs[SENSOR_MAX_SENSORS_CONFIGURED];
static uint8_t num_sensors_configured = 0;


#if SENSIML_INTERFACE_ENABLE_COLLECTION
/* return list of supported sensors */
static void get_list(Mqttsn_IOMsgData_t *pIoMsgData)
{
    //reply with a Publish payload
    int result = 0;
    Mqttsn_MsgData_t *pOutMsgData = &pIoMsgData->outgoingResponse;

    result = Mqttsn_AllocPublishPayload(MQTTSN_BUFFER_SMALL, &pOutMsgData->pMsgPayldBuf,
                                        &pOutMsgData->allocLen);

    if (result)
    {
        sensor_user_get_list(pOutMsgData);
    }
}

/* dispatch entry */
struct sensor_cmd_dispatch_entry
{
    uint32_t value;
    void (*handler)(Mqttsn_IOMsgData_t *);
    uint32_t expPayldLength;
    uint8_t isVariableLen;
};

static void wrap_sensor_clear(Mqttsn_IOMsgData_t *pIoMsgData)
{
    sensor_user_clear();
    sensor_user_all_startstop(0);
    /* The last action is to clear the sensor configuration data internally */
    memset((void*)(sensor_configs), 0, SENSOR_MAX_SENSORS_CONFIGURED * sizeof(struct sensor_config_msg));
}

static void wrap_sensor_add(Mqttsn_IOMsgData_t *pIoMsgData)
{
    uint8_t *pInBuff = pIoMsgData->incomingMsg.pMsgPayldBuf;

    recent_cfg_msg.sensor_common.sensor_id = Mqttsn_BuffRead_u32(&pInBuff);
    recent_cfg_msg.sensor_common.rate_hz = Mqttsn_BuffRead_u32(&pInBuff);

    memcpy((void *)&recent_cfg_msg.unpacked, pInBuff, pIoMsgData->incomingMsg.msgLen - 8);

    if (num_sensors_configured < SENSOR_MAX_SENSORS_CONFIGURED)
    {
        memcpy((void*) &sensor_configs[num_sensors_configured], &recent_cfg_msg, sizeof(struct sensor_config_msg));
        sensor_user_add((const struct sensor_config_msg*)&recent_cfg_msg);
        num_sensors_configured++;
    }
    else
    {
        set_sys_error(SYS_ERR_EINVAL, recent_cfg_msg.sensor_common.sensor_id);
    }


}

#define CONFIG_LENGTH_MIN 1 //IMU has 2 byte config value

/* sensor commands we support - in a table */
static struct sensor_cmd_dispatch_entry const scde_table[] = {
    {.value = TOPIC_SENSOR_LIST_REQ, get_list, .expPayldLength = 0, .isVariableLen = 0},
    {.value = TOPIC_SENSOR_CLEAR, wrap_sensor_clear, .expPayldLength = 0, .isVariableLen = 0},
    {.value = TOPIC_SENSOR_ADD, wrap_sensor_add, .expPayldLength = 8 + CONFIG_LENGTH_MIN, .isVariableLen = 1},
    {.value = TOPIC_SENSOR_DONE, sensor_user_configure_all, .expPayldLength = 0, .isVariableLen = 0},
    /* terminate */
    {.value = -1, .handler = NULL}};

/* process the SENSOR major command
 * really all we do is dispatch the sub-command.
 */
void run_sensor_cmd(Mqttsn_IOMsgData_t *pIoMsgData)
{
    Mqttsn_MsgData_t *pInMsg = &pIoMsgData->incomingMsg;

    const struct sensor_cmd_dispatch_entry *pSCDE;

    //if its a PUBACK, we have no use for it here just return
    if (pInMsg->msgType == MQTTSN_PUBACK)
        return;

    recent_cfg_msg.msg_type = GET_TOPIC_MINOR(pInMsg->topicId);

    for (pSCDE = scde_table; pSCDE->handler != NULL; pSCDE++)
    {
        if (pSCDE->value == pInMsg->topicId)
        {
            uint32_t payldErr = CheckPayloadValidity(&pIoMsgData->incomingMsg,
                                                     pSCDE->expPayldLength,
                                                     pSCDE->isVariableLen);

            if (!payldErr)
            {
                (*(pSCDE->handler))(pIoMsgData);
                return;
            }
            else
            {
                //there is a payload error, send an error message back
                PopulateRejectMsg(pIoMsgData, SYS_ERR_EINVAL);
            }
        }
    }
    /* unknown */
    set_sys_error(SYS_ERR_ENOTSUP, pInMsg->topicId);
}

#endif //SENSIML_INTERFACE_ENABLE_COLLECTION
