/*==========================================================
*
*-  Copyright Notice  -------------------------------------
*
*    Licensed Materials - Property of QuickLogic Corp.
*    Copyright (C) 2019 QuickLogic Corporation
*    All rights reserved
*    Use, duplication, or disclosure restricted
*
*    File   : MinorTopicDispatch_Recog.c
*    Purpose: Recognition minor topic dispatch
*
*=========================================================*/
#include <string.h>
#include "Mqttsn_globals.h"
#include "MajorTopicDispatch.h"
#include "Mqttsn_MessageHandler.h"
#include "MinorTopicDispatch_Collect.h"
#include "Mqttsn_Topics.h"
#include "CollectTopicUser.h"

/* This will not be used if we doo not support Data storage */
#if SENSIML_CAPTURE_CAN_COLLECT_FLASH_SD

struct file_save_config;

//Predeclarations
static void collect_set_filename(const char *pFileName);
static void do_set_collect_prefix(Mqttsn_IOMsgData_t *pIoMsgData);
static void do_collect_stop(Mqttsn_IOMsgData_t *pIoMsgData);
static void do_collect_start(Mqttsn_IOMsgData_t *pIoMsgData);

static void collect_set_filename(const char *pFileName)
{
    /* this is the prefix for the filenames we create */
    strncpy(file_save_config.cur_filename_template,
            pFileName,
            sizeof(file_save_config.cur_filename_template) - 1);
    /* force terminate */
    file_save_config.cur_filename_template[sizeof(file_save_config.cur_filename_template) - 1] = 0;
}

static void do_set_collect_prefix(Mqttsn_IOMsgData_t *pIoMsgData)
{
    char *pStorageLocation = "/default/";
    uint32_t storageLocLen = strlen((const char *)pStorageLocation);
    uint8_t *pPrefix = pIoMsgData->incomingMsg.pMsgPayldBuf;
    uint8_t *pFilename = NULL;

    if ((strncmp("/", (const char *)pPrefix, 1) == 0))
    {
        if (strncmp((const char *)pStorageLocation, (const char *)pPrefix, storageLocLen) == 0)
        {
            pFilename = pPrefix + storageLocLen;
        }
        else
        {
            //anything other than /default/ is not supported
            //printf("anything other than /default/ is not supported\n");
        }
    }
    else
    {
        pFilename = pPrefix;
    }

    if (pFilename != NULL)
    {
        collect_set_filename((char const *)pFilename);
    }
}

static void do_collect_stop(Mqttsn_IOMsgData_t *pIoMsgData)
{
    int stopped = collect_user_data_start_stop(0, NULL, 0);

    if (stopped == 0)
    {
        mqttsn_globals.cur_status.bit_flags &= ~COLLECT_ACTIVE;
    }
}

void do_collect_start(Mqttsn_IOMsgData_t *pIoMsgData)
{
    /* guids are 16 bytes */
    int did_start = collect_user_data_start_stop(1, pIoMsgData->incomingMsg.pMsgPayldBuf, GUID_BYTES_MAX);
    if (did_start == 0)
    {
        mqttsn_globals.cur_status.bit_flags |= ~COLLECT_ACTIVE;
    }
}

void Collect_AllStop(Mqttsn_IOMsgData_t *pIoMsgData)
{
    do_collect_stop(pIoMsgData);
    mqttsn_globals.cur_status.bit_flags &= ~COLLECT_ACTIVE;
}

/* collect commands  */
static struct collect_cmd_dispatch_entry const ccde_table[] = {
    {.value = TOPIC_COLLECT_PREFIX_SET, do_set_collect_prefix, .expPayldLength = MIN_PREFIX_LENGTH, .isVariableLen = 1},
    {.value = TOPIC_COLLECT_START, do_collect_start, .expPayldLength = 16, .isVariableLen = 0},
    {.value = TOPIC_COLLECT_STOP, do_collect_stop, .expPayldLength = 0, .isVariableLen = 0},
    /* terminate */
    {.value = -1, .handler = NULL}};

// process the Recognition minor command
void run_collect_cmd(Mqttsn_IOMsgData_t *pIoMsgData)
{
    Mqttsn_MsgData_t *pInMsg = &pIoMsgData->incomingMsg;

    //if its a PUBACK, we have no use for it here just return
    if (pInMsg->msgType == MQTTSN_PUBACK)
        return;

    const struct collect_cmd_dispatch_entry *pCCDE;

    for (pCCDE = ccde_table; pCCDE->handler != NULL; pCCDE++)
    {
        if (pCCDE->value == pInMsg->topicId)
        {
            uint32_t payldErr = CheckPayloadValidity(&pIoMsgData->incomingMsg,
                                                     pCCDE->expPayldLength,
                                                     pCCDE->isVariableLen);

            if (!payldErr)
            {
                (*(pCCDE->handler))(pIoMsgData);
                return;
            }
            else
            {
                //there is a payload error, send an error message back
                PopulateRejectMsg(pIoMsgData, SYS_ERR_EINVAL);
            }
        }
    }

    /* unknown */
    set_sys_error(SYS_ERR_ENOTSUP, pInMsg->topicId);
}

#endif //SENSIML_CAPTURE_CAN_COLLECT_FLASH_SD
