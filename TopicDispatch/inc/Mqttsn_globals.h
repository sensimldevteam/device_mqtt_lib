#ifndef __MQTTSN_GLOBALS_H__
#define __MQTTSN_GLOBALS_H__

#include <stdint.h>
#include <stdlib.h>
#include "Mqttsn_MessageHandler.h"
#include "MinorTopicDispatch_System.h"
#include "MqttGlobalsUser.h"

#if !defined(FALSE)
#define FALSE 0
#endif //FALSE
#if !defined(TRUE)
#define TRUE 1
#endif //TRUE

/******************* DEFAULT DEFINES *****************************
*
* Here the user defines if firmware supports Collection,
* Recognition, or both.
***************************************************************/
#ifndef SENSIML_INTERFACE_ENABLE_RECOGNITION
#define SENSIML_INTERFACE_ENABLE_RECOGNITION 0
#endif //SENSIML_INTERFACE_ENABLE_RECOGNITION
#ifndef SENSIML_INTERFACE_ENABLE_COLLECTION
#define SENSIML_INTERFACE_ENABLE_COLLECTION 1
#endif //SENSIML_INTERFACE_ENABLE_COLLECTION

#define MQTTSN_MAX_BYTES 128


#define SENSOR_MAKE_ID_32BIT(A, B, C, D) \
    (((A) << 24) + ((B) << 16) + ((C) << 8) + ((D) << 0))

//TODO [Justin] : Change the name to result_class_type
enum sensiml_classification_type
{
    CLASS_RESULT_ONLY = 1, // type (uint8_t) + current time (uint64_t) + model(uint16_t) index + class_result(uint16_t)
    CLASS_RESULT_FV = 2,   // _ONLY + number of feature vectors (uint8_t) + vectors(number of VF, each VF is 1 byte)
    /* New type must be added before this line. Update CLASS_RESULT_MAX to be the last type. */
    CLASS_RESULT_MAX = CLASS_RESULT_FV //The device is busy and cannot do this
};

struct mqtt_sn_raw_msg
{
    uint8_t len;
    uint8_t cmd; /* one of iop_msg_cmd_t */
    uint8_t payload[126];
};

/*
 * Some replies are simple, we use this structure for generic replies.
 * Example:  VERSIONSTRING reply uses this, as do a few others.
 */
struct mqttsn_misc_reply
{
    /* the command replying to */
    uint8_t cmd;
    union {
        /* data is a string or bytes */
        char cbuf[100];
        uint8_t as_u8[100];
    } u;
};

struct mqtt_xyz_data
{
    uint16_t x, y, z;
};

struct mqtt_imu_data
{
    /* this dummy value is here for alignment
    * and is special cased & removed later */
    uint8_t dummy_value;
    uint8_t n_values;
    struct mqtt_xyz_data motion[12];
};

#if SENSIML_INTERFACE_ENABLE_RECOGNITION

#ifndef MAX_VECTOR_SIZE
#define MAX_VECTOR_SIZE 128
#define MAX_FEATURE_VECTOR_FRAME_SZ MAX_VECTOR_SIZE
#else
#define MAX_FEATURE_VECTOR_FRAME_SZ 2 //not for collection.
#endif                                //MAX_VECTOR_SIZE

typedef struct
{
    uint16_t context;
    uint16_t classification;

    uint8_t fv_len;                                      //Actual length to read
    uint8_t feature_vector[MAX_FEATURE_VECTOR_FRAME_SZ]; //Max features reporting out is 128

} sensiml_result_fv_t;

typedef struct
{
    uint16_t context;
    uint16_t classification;
} sensiml_result_t;
#endif //#if SENSIML_INTERFACE_ENABLE_RECOGNITION

/**
 * @brief These are the task variables used by the MQTTSN library
 */
struct mqttsn_globals
{
    struct mqtt_sn_raw_msg cmd_from_host;
    struct mqtt_sn_raw_msg rsp_to_host;
    struct sys_status cur_status;

    int data_busy;
    /**
     * @brief Output ddta messages for sensor and classification messages.
     */
    union mqttsn_data_msg {
        uint8_t as_u8[MQTTSN_MAX_BYTES];
        char as_string[MQTTSN_MAX_BYTES];
        uint32_t as_u32[MQTTSN_MAX_BYTES / 4];
        uint16_t as_u16[MQTTSN_MAX_BYTES / 2];
#if SENSIML_INTERFACE_ENABLE_COLLECTION
        struct mqtt_imu_data imu_data;
#endif //SENSIML_INTERFACE_ENABLE_COLLECTION
#if SENSIML_INTERFACE_ENABLE_RECOGNITION
        sensiml_result_t pme_results;
        sensiml_result_fv_t pme_fv_results;
#endif //SENSIML_INTERFACE_ENABLE_RECOGNITION
    } u_data;

    /**
     * @brief Commands and response messages.
     *
     */
    union mqttsn_aligned_msg {
        uint8_t as_u8[MQTTSN_MAX_BYTES];
        char as_string[MQTTSN_MAX_BYTES];
        uint32_t as_u32[MQTTSN_MAX_BYTES / 4];
        uint16_t as_u16[MQTTSN_MAX_BYTES / 2];
        struct mqttsn_misc_reply misc;
    } u_cmd, u_rsp;

    uint8_t class_type;
    uint8_t class_rate;
    uint8_t class_count;
};

extern struct mqttsn_globals mqttsn_globals;

#endif //__MQTTSN_GLOBALS_H__
