#ifndef __MINOR_TOPIC_DISPATCH_COLLECT_H__
#define __MINOR_TOPIC_DISPATCH_COLLECT_H__
#include "CollectTopicUser.h"

#define GUID_BYTES_MAX 16

#if !defined(MAX_AUDIO_MICROPHONE)
#define MAX_AUDIO_MICROPHONE 4
#endif //MAX_AUDIO_MICROPHONE

#if !defined(FS_MAX_ABS_PATHLEN)
/* Entire filename, volume + path + filename */
#define FS_MAX_ABS_PATHLEN (200 + 1)
#endif //FS_MAX_ABS_PATHLEN
#if !defined(FS_MAX_FILENAME_COMPONENT)
/* no component of the filename can be larger then this */
#define FS_MAX_FILENAME_COMPONENT 50
#endif //FS_MAX_FILENAME_COMPONENT

struct file_save_config
{
    /*
   * How are we saving data
   */

    /* base filename */
    char cur_filename_template[20];
    /* computed filename (with timestamp as ascii) */
    char cur_filename[FS_MAX_ABS_PATHLEN];

    /* option flags from host, today options are 0 nothing.
   * future might select USB or SPI instead of SD CARD.
   */
    uint32_t optionflags;

    /* GUID to from the start command to be written to the JSON header */
    uint8_t guid_bytes[GUID_BYTES_MAX];
};

extern struct file_save_config file_save_config;

struct audio_config
{
    int is_running;
    int n_channels_enabled;
    uint32_t sample_rate_hz;
    int nbits;
    /* if mic_config==0 disabled.
    * if mic_config==1 normal operation
    * otherwise - "some custom configuration" TBD.
    */

    uint8_t mic_config[MAX_AUDIO_MICROPHONE];
};

struct collect_cmd_dispatch_entry
{
    uint32_t value;
    void (*handler)(Mqttsn_IOMsgData_t *);
    uint32_t expPayldLength;
    uint8_t isVariableLen;
};

void run_collect_cmd(Mqttsn_IOMsgData_t *pIoMsgData);
void Collect_AllStop(Mqttsn_IOMsgData_t *pIoMsgData);

#endif //__MINOR_TOPIC_DISPATCH_COLLECT_H__