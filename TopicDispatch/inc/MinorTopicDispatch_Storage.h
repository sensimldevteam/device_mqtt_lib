#ifndef __MINOR_TOPIC_STORAGE_H__
#define __MINOR_TOPIC_STORAGE_H__

#include "Mqttsn_MessageHandler.h"
/**
 * @brief run_storage_cmd - Parses storage minor topics, selects appropriate handler
 *
 * @param pIoMsgData - MqttSN Message In
 */
void run_storage_cmd(Mqttsn_IOMsgData_t *pIoMsgData);

/**
 * @brief Stops all storage (GET/PUT) operations on device.
 *
 */
void Storage_AllStop(void);

#endif //__MINOR_TOPIC_STORAGE_H__
