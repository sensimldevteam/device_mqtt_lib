#ifndef __MAJOR_TOPIC_DISPATCH_H__
#define __MAJOR_TOPIC_DISPATCH_H__

#include <stdio.h>
#include <stdint.h>
#include "Mqttsn_globals.h"
#include "Mqttsn_MessageHandler.h"
#include "MinorTopicDispatch_System.h"

typedef struct
{
    uint8_t value;
    void (*handler)(Mqttsn_IOMsgData_t *);
} SensiMLMajorTpcDisptach_t;

/******************* Function Declarations*********************/

/**
 * @brief run_system_cmd - Runs System Commands from MQTTSN
 *
 * @param pIoMsgData - MqttSN Message In
 */
void run_system_cmd(Mqttsn_IOMsgData_t *pIoMsgData);

/**
 * @brief run_livestream_cmd - Parses Live stream minor topics, selects appropriate handler
 *
 * @param pIoMsgData - MqttSN Message In
 */
void run_livestream_cmd(Mqttsn_IOMsgData_t *pIoMsgData);

/**
 * @brief run_recognition_cmd - Parses recognition minor topics, selects appropriate handler
 *
 * @param pIoMsgData - MqttSN Message In
 */
void run_recognition_cmd(Mqttsn_IOMsgData_t *pIoMsgData);

/**
 * @brief run_collect_cmd - Parses collection minor topics, selects appropriate handler
 * Collection Methods include
 * - Setting File Name
 * - Start/Stop Data Collection
 * - Selecting Sensors to Collect
 * @param pIoMsgData - MqttSN Message In
 */
void run_collect_cmd(Mqttsn_IOMsgData_t *pIoMsgData);

void mqtt_reply_string(uint8_t *pBuf, const char *cp, ...);

void run_sensor_cmd(Mqttsn_IOMsgData_t *pIoMsgData);
void set_sys_error(sys_error_code_t errcode, uint32_t more_info);
uint32_t CheckPayloadValidity(Mqttsn_MsgData_t *pInputMsg, uint32_t expPayldLen, uint8_t isVariableLen);
void PopulateRejectMsg(Mqttsn_IOMsgData_t *pIoMsgData, sys_error_code_t ftErr);
int send_data_start(void);
void send_data_end(void);
void MajorTopicDispatch(Mqttsn_IOMsgData_t *pIoMsgData);

#endif //__MAJOR_TOPIC_DISPATCH_H__
