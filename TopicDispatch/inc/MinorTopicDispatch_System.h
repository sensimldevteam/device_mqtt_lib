#ifndef __MINOR_TOPIC_SYSTEM_H__
#define __MINOR_TOPIC_SYSTEM_H__
#include "Mqttsn_globals.h"
#include "Mqttsn_MessageHandler.h"
#include "SystemTopicUser.h"

/**
 * @brief System Error codes as defined in SensiML Interface Spec.
 *
 */
typedef enum
{
    SYS_ERR_NO_ERROR = 0,
    SYS_ERR_EPERM = 1,      //permission denied (ie: read only)
    SYS_ERR_ENOENT = 2,     //file not found
    SYS_ERR_EIO = 5,        //IO Error (the sdcard disappeared)
    SYS_ERR_EBUSY = 16,     //The device is busy and cannot do this
    SYS_ERR_ENODEV = 19,    //That storage area does not exist, or that sensor does not exist.
    SYS_ERR_EINVAL = 22,    //something is invalid in the request (preferred over ERANGE)
    SYS_ERR_EFBIG = 27,     //file is too big
    SYS_ERR_EROFS = 30,     //read only file system
    SYS_ERR_ETIMEDOUT = 60, //Timeout
    SYS_ERR_ESTALE = 70,    //There is no transfer going on (invalid transaction id)
    SYS_ERR_ENOSYS = 78,    //Feature not implemented
    SYS_ERR_ENOMEDIUM = 85, //No medium found
    SYS_ERR_ENOTSUP = 91    //The request is not supported
} sys_error_code_t;

struct system_cmd_dispatch_entry
{
    uint32_t value;
    void (*handler)(Mqttsn_IOMsgData_t *);
    uint32_t expPayldLength;
    uint8_t isVariableLen;
};

__packed struct sys_status
{
    /* number of bytes written to usb/sdcard */
    uint32_t bytes_saved;

    /* bit values for status bit s*/
#define COLLECT_ACTIVE 0x0001        /* device is storing data to filesystem */
#define LIVESTREAM_ACTIVE 0x0002     /* sending IMU data to the BLE */
#define IOP_STATUS_BIT_reco 0x0008   /* recognition is enabled */
#define IOP_STATUS_BIT_reco_f 0x0010 /* recongition with features are enabled */
#define LIVESTREAM_OVERRUN 0x0080    /* BLE cannot keep up with the data, overrun error */
#define COLLECT_OVERRUN 0x0100       /* SD card cannot keep up, overr runrun error occured */
#define ANY_ERROR 0x0200             /* set if ANY error occured */
    uint32_t bit_flags;

    /* packet counters, always increasing - DCL can use this to monitor health of BLE connection */
    uint16_t rx_count;
    uint16_t tx_count;

    /* overrun counters, see the "oe" status bits above */
    uint16_t live_oe_count;
    uint16_t collect_oe_count;

    /* sticky error number, only saves first, until an ERROR clear occurs */
    uint8_t sticky_error_code;

    /* monotonic increasing error counter,
     * DCL can use this to determine if errors occured
     * and if more then 1 error occured */
    uint8_t error_count;
};

extern const char *SOFTWARE_VERSION_STR;
#endif //__MINOR_TOPIC_SYSTEM_H__
