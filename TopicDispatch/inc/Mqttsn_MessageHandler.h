#ifndef __MQTTSN_MESSAGEHANDLER_H
#define __MQTTSN_MESSAGEHANDLER_H

#include <stdint.h>
#if defined(USES_FREERTOS) || defined(FreeRTOS)
#include "FreeRTOS.h"
#endif //#if defined(USES_FREERTOS)
#include "MQTTSNPacket.h"
#include "MQTTSNConnect.h"
#include "Mqttsn_Topics.h"
#include "Mqttsn_Message_Types.h"

extern const char *SOFTWARE_VERSION_STR;

#define UUID_TOTAL_BYTES     16
extern uint8_t DeviceClassUUID[UUID_TOTAL_BYTES];


/************************************APIs**************************************/
#if defined(USES_FREERTOS) || defined(FreeRTOS)
extern signed portBASE_TYPE StartRtosTaskMqttsnMsgHandler(void);
#endif //#if defined(USES_FREERTOS)

uint32_t Mqttsn_CopyMsg(Mqttsn_MsgData_t *pMsgData, uint8_t *pBuf, uint32_t numBytes);
uint32_t Mqttsn_ProcessIncmgMsg(Mqttsn_IOMsgData_t *pIoMsgData, MqttsnPdu_t *pMqttsnPdu);
uint32_t Mqttsn_AllocMsg(Mqttsn_BuffSz_t buffSz, uint8_t **pBuf, uint32_t *pBufLen);
uint32_t Mqttsn_AllocPublishPayload(Mqttsn_BuffSz_t buffSz, uint8_t **pBuf, uint32_t *pBufLen);
uint32_t Mqttsn_SendReply(Mqttsn_MsgData_t *pMsgData);
Mqttsn_TxPublishErrorCodes_t Mqttsn_SendPublish(Mqttsn_MsgData_t *pMsgData, Mqttsn_TopicInfo_t *pTopicInfo);
void Mqttsn_FreePublishPayload(uint8_t **pBuf, uint32_t allocLen);
uint32_t Mqttsn_FreeMsg(uint8_t **pBuf);
uint32_t Mqttsn_Subscribe(Mqttsn_TopicInfo_t *pTopicInfo);
uint32_t Mqttsn_Register(Mqttsn_TopicInfo_t *pTopicInfo);
uint32_t Mqttsn_SendConnectionRequest(void);
extern void SendMessage(uint8_t *pBuf, uint32_t bufLen);
uint32_t Mqttsn_BuffWr_u64(uint8_t **pBuf, uint64_t v);
uint32_t Mqttsn_BuffWr_u32(uint8_t **pBuf, uint32_t v);
uint32_t Mqttsn_BuffWr_u16(uint8_t **pBuf, uint16_t v);
uint32_t Mqttsn_BuffWr_u8(uint8_t **pBuf, uint8_t v);
uint8_t Mqttsn_BuffRead_u8(uint8_t **pBuff);
uint16_t Mqttsn_BuffRead_u16(uint8_t **pBuff);
uint32_t Mqttsn_BuffRead_u32(uint8_t **pBuff);
void Mqttsn_PublishWillStatus(Mqttsn_WillStatus_t willStatus);
uint32_t GetPayloadOffset(uint32_t msgLength);
#endif //MQTTSN_H
