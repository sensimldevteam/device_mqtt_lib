#ifndef __MINOR_TOPIC_DISPATCH_SENSOR_H__
#define __MINOR_TOPIC_DISPATCH_SENSOR_H__

#include "SensorTopicUser.h"

#if !defined(SENSOR_MAX_NUMSENSORS_ON_DEVICE)
#define SENSOR_MAX_NUMSENSORS_ON_DEVICE 8
#endif //SENSOR_MAX_NUMSENSORS_ON_DEVICE
#define SENSOR_RESERVED 0xFFFFFFFF


/**
 * @brief This is the sensor configuration common to every sensor
 * that uses the SensiML Interface specification
 */
typedef struct
{
    uint32_t sensor_id;
    uint32_t rate_hz; /* rate = 0 is disabled */
} sensor_common_t;

/**
 * @brief
 *
 */
typedef struct
{
    /**
    * Range * 10,
    *   ie: 20 = 2g, 40 = 4g, 5 = 0.5 g
    */
    uint8_t sensor1_range, sensor2_range;
} sensor_imu_t;

typedef struct
{
    /* data format is an array of u16 values, no compression
    * Rate is in the rate_hz field.
    *
    * To configure mono, set mic_config[0] = 1
    * To configure stereo set both [0] and [1] to 1
    *
    * Normally only 0, 1, or 2 mics are supported.
    * Some platforms may have 3 or 4 mics
    * Rarely do we expect to see more then 4
    *
    * not all sample rates are supported.
    * not all mic configurations are supported.
    *
    * mic config = 0 is disabled.
    * mic config = 1 is "normal default" operation.
    *
    * If a MIC supports other ranges ... ie: pre-amp
    * then that is different value (not 1, and not 0)
    */
    uint8_t nbits; /* 8 or 16 */
    /* While S3 supports 4
     * the packet supports 8 (future chips)
     */
    uint8_t mic_config[8];
} sensor_audio_t;

typedef struct
{
    uint8_t chnl_config[8]; /* 8bytes */
} sensor_adc_ltc1859_a_t;

/**
 * @brief This is the common structure of a sensor config message
 * Note: some of these items are legacy from QuickAI implementation
 *
 * Typically, when reading in sensor configuration with a message,
 * sensor_config_msg.unpacked.as_u8 will be used.
 */
struct sensor_config_msg
{
    uint8_t msg_type; /* really: sensor_config_cmd_t */
    sensor_common_t sensor_common;

    union sensor_union {
        uint8_t as_u8[32];
        sensor_imu_t imu_config;
        //TODO will define types for accel,gyro,mag separately.
        sensor_adc_ltc1859_a_t ltc1859_a;
        sensor_audio_t audio;
    } unpacked;
};

extern struct sensor_config_msg recent_cfg_msg;
extern struct sensor_config_msg sensor_configs[SENSOR_MAX_SENSORS_CONFIGURED];
extern const struct sensor_config_msg datacapture_default_config[];

#endif //__MINOR_TOPIC_DISPATCH_SENSOR_H__
