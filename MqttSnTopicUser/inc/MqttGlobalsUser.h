#ifndef _MQTT_GLOBALS_USER_H__
#define _MQTT_GLOBALS_USER_H__

/******************* USER DEFINES *****************************
*
* Here, the user defines supported features that they have
* implemented.
***************************************************************/

/**
 * @brief Enables Recognition mode for SensiML Models
 */
#define SENSIML_INTERFACE_ENABLE_RECOGNITION 0

/**
 * @brief Enables Data Collection mode
 */
#define SENSIML_INTERFACE_ENABLE_COLLECTION 1

/**
 * @brief Indicates that system has Flash OR SD Storage for data collection
 * See TOPIC_COLLECT in SensiML Interface Spec
 */
#define SENSIML_CAPTURE_CAN_COLLECT_FLASH_SD 1

/**
 * @brief Used to indicate file transfer operations are desired.
 * See TOPIC_STORAGE in SensiML Interface Spec
 */
#define SENSIML_SYSTEM_SUPPORTS_FILE_TRANSFER 0

/**
 * @brief Used to indicate the system can live stream data
 * Implements the TOPIC_LIVE section of SensiML Interface Spec
 */
#define SENSIML_SYSTEM_CAN_LIVE_STREAM_DATA 1

#endif //_MQTT_GLOBALS_USER_H__
