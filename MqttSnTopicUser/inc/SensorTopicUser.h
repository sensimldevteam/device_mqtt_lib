#ifndef __SENSOR_TOPIC_USER_H__
#define __SENSOR_TOPIC_USER_H__
#include <stdbool.h>
#include <string.h>
#include "Mqttsn_globals.h"
#include "Mqttsn_MessageHandler.h"

/*************EXAMPLE SENSOR ID DEFINES *****************/
/* About SENSOR_ENG_VALUE_IMU_agm - it is only used in "get list of supported sensors".
* and when the AGM sensors are ganged
*
* The low 8bits represents which sensor is present (set=1) or not present (clear=0)
* bits 0 - accel
* bits 1 - gyro
* bits 2 - magnetometer
*/
#define SENSOR_ENG_VALUE_IMU_agm_base SENSOR_MAKE_ID_32BIT('I', 'M', 'U', 0)
#define SENSOR_ENG_VALUE_IMU_agm_a_bit (1)
#define SENSOR_ENG_VALUE_IMU_agm_g_bit (2)
#define SENSOR_ENG_VALUE_IMU_agm_m_bit (4)
#define SENSOR_ENG_VALUE_ACCEL SENSOR_MAKE_ID_32BIT('I', 'M', 'U', 'A')
#define SENSOR_ENG_VALUE_GYRO SENSOR_MAKE_ID_32BIT('I', 'M', 'U', 'G')
#define SENSOR_ENG_VALUE_MAGNETOMETER SENSOR_MAKE_ID_32BIT('I', 'M', 'U', 'M')

#define SENSOR_ENG_VALUE_ACCEL_GYRO (SENSOR_ENG_VALUE_IMU_agm_base \
                                        + SENSOR_ENG_VALUE_IMU_agm_a_bit \
                                        + SENSOR_ENG_VALUE_IMU_agm_g_bit)

#define SENSOR_ENG_VALUE_ACCEL_GYRO_MAG (SENSOR_ENG_VALUE_IMU_agm_base \
                                        + SENSOR_ENG_VALUE_IMU_agm_a_bit \
                                        + SENSOR_ENG_VALUE_IMU_agm_g_bit \
                                        + SENSOR_ENG_VALUE_IMU_agm_m_bit)

/* Forward declared sensor_config_msg struct */
struct sensor_config_msg;

/**
 * @brief Determines the maximum number of Sensors used by the system.
 * 8 will be used if none specified here.
 */
#define SENSOR_MAX_NUMSENSORS_ON_DEVICE 8

#define SENSOR_MAX_SENSORS_CONFIGURED SENSOR_MAX_NUMSENSORS_ON_DEVICE

/**
 * @brief Build a list of available sensor ID's, placing them in the output data buffer.
 *
 * @param[OUT] pOutMsgData Output Data buffer
 */
extern void sensor_user_get_list(Mqttsn_MsgData_t *pOutMsgData);

/**
 * @brief This is the function used to add and configure sensors for data collection
 *
 */
extern void sensor_user_add(const struct sensor_config_msg *pCfg);

/**
 * @brief This is the function to clear the configured sensor list
 * Can be used to also "shut down" sensors/go into lower power mode.
 *
 */
extern void sensor_user_clear(void);

/**
 * @brief This function will start/enable all sensors, or disable them all
 *
 * @param is_start 1 for start, 0 for stop.
 */
extern void sensor_user_all_startstop(int is_start);

/**
 * @brief Function to configure from a premade list of sensor configurations
 *
 * @param pIoMsgData
 */
extern void sensor_user_configure_all();

#endif //__SENSOR_TOPIC_USER_H__
