#ifndef __LIVE_TOPIC_USER_H__
#define __LIVE_TOPIC_USER_H__
#include "Mqttsn_Message_Types.h"

extern void live_user_set_default(void);
extern void user_livestream_sensor_list_req(Mqttsn_IOMsgData_t* pIoMsgData);

#endif //__LIVE_TOPIC_USER_H__
