#ifndef __COLLECT_TOPUC_USER_H__
#define __COLLECT_TOPUC_USER_H__

/* Entire filename, volume + path + filename */
/**
 * @brief The maximum path length allowed, including filename,
 * directory, etc.
 */
#define FS_MAX_ABS_PATHLEN (200 + 1)

/**
 * @brief Longest file name allowed
 */
#define FS_MAX_FILENAME_COMPONENT 50
#define MAX_AUDIO_MICROPHONE 4
#define MIN_PREFIX_LENGTH 1

/**
 * @brief User function to start/stop data collection on storage medium
 *
 * @param is_start 0 to stop, 1 to start collection
 * @param pGuid Pointer to GUID byte array, stored in file for identification
 * @param guid_len Length of pGuid array, in bytes.
 * @return int - 0 for OK, Err otherwise.
 */
extern int collect_user_data_start_stop(int is_start, const uint8_t* pGuid, size_t guid_len);

#endif //__COLLECT_TOPUC_USER_H__
