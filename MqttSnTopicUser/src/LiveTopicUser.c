#include "LiveTopicUser.h"
#include "MinorTopicDispatch_Live.h"
#include "MinorTopicDispatch_Sensor.h"
#include "SensorTopicUser.h"

mqttsn_sensor_live_t sensor_live_list[] = {
    // USER TODO: Fill in sensor list for supported live-stream sensors.
    // SENSOR_LIVE_INIT(SENSOR_ENG_VALUE_ACCEL, 0, 1, 6, 0, 0, 0, 0, 0),
    // SENSOR_LIVE_INIT(SENSOR_ENG_VALUE_GYRO, 0, 1, 6, 0, 0, 0, 0, 0),
    // SENSOR_LIVE_INIT(SENSOR_ENG_VALUE_ACCEL_GYRO, 0, 1, 12, 0, 0, 0, 0, 0),
    // //FIXME sample sizes of following sensors need to be douple checked.
    // SENSOR_LIVE_INIT(SENSOR_ENG_VALUE_MAGNETOMETER, 0, 1, 2, 0, 0, 0, 0, 0),
    // SENSOR_LIVE_INIT(SENSOR_AUDIO, 0, 1, 2, 0, 0, 0, 0, 0),
    // SENSOR_LIVE_INIT(SENSOR_ADC_LTC_1859_MAYHEW, 0, 1, 2, 0, 0, 0, 0, 0),
    // SENSOR_LIVE_INIT(SENSOR_ADC_LTC_1859_B, 0, 1, 2, 0, 0, 0, 0, 0),

    /* reserved for new sensor */
    // If new sensor needs to be added, add it before this line.
    // The last line should always be SENSOR_RESERVED
    SENSOR_LIVE_INIT(SENSOR_RESERVED, 0, 1, 1, 0, 0, 0, 0, 0)
};

void live_user_set_default(void)
{
    mqttsn_sensor_live_t* s = &sensor_live_list[0];

    do {
        /* sample size */
        switch (s->sensor_id) {
            /* USER TODO: switch on sensor ID's, handle setting default live rate */
        default:
            break;
        }

        /* common */
        SENSOR_LIVE_DISABLE(s);
        SENSOR_LIVE_SET_MIN(s, 1);
        SENSOR_LIVE_CLR_SEQ(s);
        SENSOR_LIVE_SET_CUR(s, 0);
        SENSOR_LIVE_SET_COUNT(s, 0);
        SENSOR_LIVE_SET_RELOAD(s, 0);
        // FIXME zero msg should only be done if allocated == 0
        // otherwise do it in ble_send()
        if (!SENSOR_LIVE_GET_ALLOC(s)) {
            memset((void*)&s->msg, 0, sizeof(Mqttsn_MsgData_t));
        }

        s++;
    } while (s->sensor_id != SENSOR_RESERVED);
}

void user_livestream_sensor_list_req(Mqttsn_IOMsgData_t* pIoMsgData)
{
    // USER TODO: build live sensor list. See QuickAI SDK implementation for
    // example
}
