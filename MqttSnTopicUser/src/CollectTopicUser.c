#include <string.h>
#include "Mqttsn_globals.h"
#include "Mqttsn_MessageHandler.h"
#include "MinorTopicDispatch_Collect.h"
#include "Mqttsn_Topics.h"
#include "CollectTopicUser.h"

#if SENSIML_CAPTURE_CAN_COLLECT_FLASH_SD
int collect_user_data_start_stop(int is_start, const uint8_t *pGuid, size_t guid_len)
{
    //USER TODO: Call your function, be it RTOS task message, etc, to start/stop a file save.

    return 0;
}
#endif //#if SENSIML_CAPTURE_CAN_COLLECT_FLASH_SD