#include "SystemTopicUser.h"
#include <time.h>

static uint32_t seconds_since_epoch;

int system_user_set_time(time_t secs_since_epoch)
{
/* Below is an example using FreeRTOS From the QuickAI SDK */
#if 0

#ifdef USE_RTC
    setup_rtc_time(secs_since_epoch);
#else
    seconds_since_epoch = secs_since_epoch;
    ql_diff_ticks = xTaskGetTickCount();

     ql_sys_settime(unixTime);
    v = unixTime;
    v = v * 1000000;
    xTaskSet_uSecCount(v);
#endif

#endif // 0
    return 0;
}
