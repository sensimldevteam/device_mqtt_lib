#include "SensorTopicUser.h"

#include "MajorTopicDispatch.h"
#include "MinorTopicDispatch_Sensor.h"


void sensor_user_get_list(Mqttsn_MsgData_t* pOutMsgData)
{
    uint8_t* pPayld = pOutMsgData->pMsgPayldBuf;

    pOutMsgData->topicId = TOPIC_SENSOR_LIST_RSP;

    /*
    USER TODO: Write the list of all sensors available on your system.
    This is done by writing their 32-bit ID's to the payload pointer *pPayld
    * You can copy the ID's from a generic structure, or add from defines
    * (see SensorTopicUser.h)
    */
    pOutMsgData->payldLen = Mqttsn_BuffWr_u32(&pPayld, 0);
}

void sensor_user_add()
{

    /* USER: add your sensors and configuration handling calls here */
    switch (recent_cfg_msg.sensor_common.sensor_id) {
    default:
        set_sys_error(SYS_ERR_ENODEV, recent_cfg_msg.sensor_common.sensor_id);
        break;
    }
}

void sensor_user_clear()
{


    /* USER: TODO: Put your sensor clearing functions here
    * These functions should disable ALL sensors,
    * and clear any configuration from them. The library will clear the configuration.
    */


}

void sensor_user_all_startstop(int is_start)
{
    /* USER: TODO: Put your sensor STOPPING functions here
    * These functions should stop data from ALL sensors,
    * but NOT clear any configuration from them.
    */
}

void sensor_user_configure_all(Mqttsn_IOMsgData_t *pIoMsgData)
{
}
