# Cloning
Clone this repository and the cmake submodule with

`git clone --recurse-submodules git@bitbucket.org:sensimldevteam/device_mqtt_lib.git`

# User-implemented functions
User implemented functions are required to be implemented for the system to work.

# Compiling
To compile for a specific toolchain or CPU, you need to specify the CPU and/or tools to use

## Using CMake file
To compile with a new CMake file, create a new one in the cmake directory and call CMake:

```
mkdir build
cd build
cmake -DCMAKE_TOOLCHAIN_FILE=cmake/YOUR_NEW_CMAKE_FILE.cmake ..
```
